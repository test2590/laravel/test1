<?php // app/Http/Controllers/ImportController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\XlsxImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    
    protected const FILTER_PREFIX = 'DPO_Filter';
    protected const FILTER_SEPARATOR = ':';
    
     
    public function loadFile(Request $request)
    {
        $this->validate($request,[
            'path' => 'required|string',
        ]);
        $data = Excel::toArray([],$request->get('path'));
        $data = $this->format($data);
        $data = $this->filter($data);
        $data = $this->normalize($data);
        $data['data'] = $this->switchRowsColumns($data['data']);
        $data = $this->finishConversion($data);
        return response()->json($data, 200);
    }

    /**
     * Filter data from not important columns.
     *
     * Removes data based on filters.
     * Also removes empty rows.
     *
     * @param array $data
     *
     * @return array
     */
    protected function filter($data)
    {
        $filtered = [];
        foreach($data['header'] as $index => $value){
            if($this->hasFilter($value)){
                $data['header'][$index] = $this->stripFilter($value);
            }else{
                $filtered[] = $index;
            }
        }
        foreach($filtered as $index){
            unset($data['header'][$index]);
        }
        foreach($data['data'] as $rowIndex => $row){
            foreach($filtered as $index){
                unset($data['data'][$rowIndex][$index]);
            }
        }
        
        foreach($data['data'] as $index => $row){
            $emptyRow = true;
            foreach($row as $rowValue){
                if($rowValue) $emptyRow = false;
            }
            if($emptyRow) unset($data['data'][$index]);
        }
        return $data;
    }
    
    /**
     * Final adjustments of data array
     * 
     * Switch format to array (header,columns).
     * Remove columns with only null values.
     * Capitalize headers.
     * 
     * @param array $data
     * 
     * @return array
     */
    protected function finishConversion($data)
    {
        $finalData = [];
        foreach($data['header'] as $index => $header){
            $header = strtolower($header);
            $header = ucfirst($header);
            $column = array_filter($data['data'][$index]);
            if($column){
                $item['header'] = $header;
                $item['column'] = $column;
                $finalData[] = $item;
            }
        }
        return $finalData;
    }
    
    /**
     * Formats data
     * 
     * Only first row is array that is important.
     * Next rows are ignored because it is the second tab in xlsx.
     * 
     * First row is assigned as header
     * and the rest as data.
     * Result is associative array with respective keys.
     * 
     * @param array $data
     * 
     * @return array
     */
    protected function format($inputData)
    {
        $header = [];
        $data = [];
        foreach($inputData[0] as $key => $value){
            if($key==0){
                $header = $value;
            }else{
                $data[] = $value;
            }
        }
        $resultData['header'] = $header;
        $resultData['data'] = $data;
        return $resultData;
    }

    /**
     * Check if value has filter
     * 
     * @param string $value
     * 
     * @return int|false
     */
    protected function hasFilter($value)
    {
        $search = strtoupper(self::FILTER_PREFIX.self::FILTER_SEPARATOR);
        $value = strtoupper($value);
        if(strpos($value, $search) !== false){
            return true;
        }
        return false;
    }
    
    /**
     * Normalize data table
     * 
     * For every multivalue element (values separated with ",")
     * replace row with such element with rows for each value
     * 
     * @param array $data
     * 
     * @return $data
     */
    protected function normalize($data)
    {
        foreach($data['data'] as $row){
            foreach($row as $rowIndex => $rowElement){
                if(strpos($rowElement, ',')){
                    $values = explode(',',$rowElement);
                    foreach($values as $value){
                        $row[$rowIndex] = $value;
                        $data['data'][] = $row;
                    }
                }
            }
        }
        $data['data'] = array_values($data['data']);
        return $data;
    }
    
    /**
     * Strip filter from value
     * 
     * @param string $value
     * 
     * @return string
     */
    protected function stripFilter($value){
        return str_replace(strtoupper(self::FILTER_PREFIX.self::FILTER_SEPARATOR),'',strtoupper($value));
    }
    
    /**
     * Switch data rows with columns
     * 
     * @param array $data
     * 
     * @return array
     */
    protected function switchRowsColumns($data)
    {
        foreach ($data as $row => $columns) {
            foreach ($columns as $row2 => $column2) {
                $resultData[$row2][$row] = $column2;
            }
        }
        return $resultData;
    }
}

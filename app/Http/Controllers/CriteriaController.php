<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CriteriaController extends Controller
{
    /**
     * Load select with criteria
     * 
     * @param array $data
     * 
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function loadSelect($data)
    {
        $data = json_decode($data);
        return view('criteria/select',[
            'label' => $data->header,
            'options' => $data->column,
        ]);
    }
}

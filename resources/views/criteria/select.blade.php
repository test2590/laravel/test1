<div class="col-4">
    <label>{{ $label }}</label>
</div>
<div class="col-8"> 
    <select class="criteria-select" name="criteria[]">
    @foreach($options as $option)
    	<option>{{ $option }}</option>
    @endforeach
    </select>
</div>

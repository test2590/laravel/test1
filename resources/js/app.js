import Vue from 'vue'
window.Vue = Vue;
require('./bootstrap'); 
import UploadComponent from './components/UploadComponent.vue';
Vue.component('upload-component', UploadComponent);

const app = new Vue({
    el: '#app'
});

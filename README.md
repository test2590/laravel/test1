## Installation

1. cp .env.example .env
2. cp docker-compose.yml.dev docker-compose.yml
3. docker-compose exec web composer install
4. docker-compose exec web npm install

## Run application

1. docker-compose up

It might be required to run it up to 3 times, if database errors and warnings occur.
By default port 8000 is used. May be adjusted in docker-compose.yml file.
